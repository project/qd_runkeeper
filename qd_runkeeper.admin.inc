<?php

function qd_runkeeper_admin() {
  $items = array();
  $items['qd_runkeeper_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('qd_runkeeper_client_id', ''),
  );
  $items['qd_runkeeper_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('qd_runkeeper_client_secret', ''),
  );
  $items['qd_runkeeper_de_authorize'] = array(
    '#type' => 'submit',
    '#value' => t('De-authorize application'),
    '#submit' => array('qd_runkeeper_de_authorize_submit'),
  );
  return system_settings_form($items);
}

function qd_runkeeper_de_authorize_submit($form, $form_state) {
  try {
    $oauth2_config = qd_runkeeper_config();
    $oauth2_client = new OAuth2\RunkeeperClient($oauth2_config);
    $access_token = $oauth2_client->getAccessToken();
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }


  $data['access_token'] = $access_token;

  $options = array(
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded',
    ),
  );
  $result = drupal_http_request(QD_RUNKEEPER_DEAUTHORIZEURL, $options);
  dpm($result);

  if ($result->code != 204) {
    throw new \Exception(
      t("Failed to disconnect application. Response code: @code, Error: @result_error",
        array(
          '@result_error' => $result->error,
          '@result_code' => $result->code,
        ))
    );
  }
  else {
    drupal_set_message(t('Application successfully disconnected'));
  }
}
